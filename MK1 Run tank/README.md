# Run tank
The run tank is designed for an operating pressure of 55bar with a total volume of 8.6 lt and a safety factor of 2.5.   
## Materials 
A 6082-T6 aluminum tube is used as the wall with two CNC cut bulkheads from 7075-T6 aluminum as the top and bottom.   
Steel bolts are used in order to attach the tube to the bulkheads.   
## Development   
The development is done with Autodesk Fusion 360.  
